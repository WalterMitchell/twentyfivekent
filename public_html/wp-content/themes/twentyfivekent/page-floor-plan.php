<?php
/*
Template Name: Floor Plans Page
*/

get_header(); 

echo get_template_part('snippet-hero-image');

?>

<section class="centered building-subnav">
	<ul class="building-subnav__list">
		<li class="building-subnav__link"><a href="<?php echo home_url('building'); ?>"><h4>The Building</h4></a></li>
		<li class="building-subnav__link active"><a href="<?php echo home_url('floor-plans'); ?>"><h4>Floor Plans</h4></a></li>
		<li class="building-subnav__link"><a href="<?php echo home_url('the-team'); ?>"><h4>The Team</h4></a></li>
	</ul>
</section>


<section id="floorplan-intro" class="full-width">
	<div class="centered block__text">
		<h1><?php if ( get_field('intro_title') ) echo esc_html( get_field( 'intro_title' ) ); ?></h1>
		<p class="big"><?php if ( get_field('intro_copy') ) echo esc_html( get_field('intro_copy' ) ); ?></p>
	</div>
</section>

<section class="full-width gray block floorplan-interactive">
	<ul class="floorplan-interactive__nav">
		<li>
			<h5 class="floorplan-interactive__nav-item active" data-floor="0">All Floors</h5>
		</li>
		<li>
			<h5 class="floorplan-interactive__nav-item" data-floor="9">Rooftop Terrace</h5>
			<div class="floorplan-interactive__detail-block" data-floor="9">
				<h2>Rooftop Terrace</h2>
				<h4>11,936 RSF</h4>
				<h4>UNRIVALED VIEWS:</h4>
				<h4>MANHATTAN SKYLINE (W &amp; S)</h4>
				<h4>EAST RIVER (W &amp; S)</h4>
				<h4>DOWNTOWN BROOKLYN (E)</h4>
				<a class="button birds-eye-toggle" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-interactive__nav-item" data-floor="8">Level 8</h5>
			<div class="floorplan-interactive__detail-block" data-floor="8">
				<h2>Level 8</h2>
				<h4>54,042 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>LARGE CENTRALIZED COLLABORATIVE SPACE</h4>
				<h4>ADJACENT TERRACE TO THE NORTH</h4>
				<h4>OUTDOOR TERRACE: 4,142 SF</h4>
				<a class="button birds-eye-toggle" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-interactive__nav-item" data-floor="7">Level 7</h5>
			<div class="floorplan-interactive__detail-block" data-floor="7">
				<h2>Level 7</h2>
				<h4>57,356 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>LARGE CENTRALIZED COLLABORATIVE SPACE</h4>
				<h4>ADJACENT TERRACE TO THE SOUTH</h4>
				<h4>OUTDOOR TERRACE: 4,045 SF</h4>
				<a class="button birds-eye-toggle" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-interactive__nav-item" data-floor="6">Level 6</h5>
			<div class="floorplan-interactive__detail-block" data-floor="6">
				<h2>Level 6</h2>
				<h4>60,570 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>LARGE CENTRALIZED COLLABORATIVE SPACE</h4>
				<h4>ADJACENT TERRACE TO THE NORTH</h4>
				<h4>OUTDOOR TERRACE: 2,363 SF</h4>
				<a class="button birds-eye-toggle" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-interactive__nav-item" data-floor="5">Level 5</h5>
			<div class="floorplan-interactive__detail-block" data-floor="5">
				<h2>Level 5</h2>
				<h4>61,447 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>CENTRALIZED COLLABORATIVE SPACE</h4>
				<h4>ADJACENT TERRACES TO THE NORTH AND SOUTH</h4>
				<h4>OUTDOOR TERRACE: 4,288 SF</h4>
				<a class="button birds-eye-toggle" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-interactive__nav-item" data-floor="4">Level 4</h5>
			<div class="floorplan-interactive__detail-block" data-floor="4">
				<h2>Level 4</h2>
				<h4>66,132 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>CENTRALIZED COLLABORATIVE SPACE</h4>
				<h4>ADJACENT TERRACES TO THE NORTH AND SOUTH</h4>
				<h4>OUTDOOR TERRACE: 6,734 SF</h4>
				<a class="button birds-eye-toggle" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-interactive__nav-item" data-floor="3">Level 3</h5>
			<div class="floorplan-interactive__detail-block" data-floor="3">
				<h2>Level 3</h2>
				<h4>74,345 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>CENTRALIZED COLLABORATIVE SPACE</h4>
				<a class="button birds-eye-toggle" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-interactive__nav-item" data-floor="2">Level 2</h5>
			<div class="floorplan-interactive__detail-block" data-floor="2">
				<h2>Level 2</h2>
				<h4>58,572 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>2N: 29,327 RSF / 2S: 29,245 RSF</h4>
				<h4>ADJACENT TERRACES TO THE NORTH AND SOUTH</h4>
				<h4>OUTDOOR TERRACE: 6,734 SF</h4>
				<a class="button birds-eye-toggle" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-interactive__nav-item" data-floor="1">Ground Floor</h5>
			<div class="floorplan-interactive__detail-block" data-floor="1">
				<h2>Ground Floor</h2>
				<h4>43,792 RSF</h4>
				<h4>22 FT OPEN CEILINGS</h4>
				<h4>RETAIL &amp; PUBLIC PLAZAS</h4>
				<a class="button birds-eye-toggle" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
	</ul>

	<div class="floors-stack">
		<img class="floors-stack__image floor-1" data-floor="1" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_Ground.svg" >
		<img class="floors-stack__image floor-2" data-floor="2" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_2.svg" >
		<img class="floors-stack__image floor-3" data-floor="3" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_3.svg" >
		<img class="floors-stack__image floor-4" data-floor="4" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_4.svg" >
		<img class="floors-stack__image floor-5" data-floor="5" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_5.svg" >
		<img class="floors-stack__image floor-6" data-floor="6" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_6.svg" >
		<img class="floors-stack__image floor-7" data-floor="7" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_7.svg" >
		<img class="floors-stack__image floor-8" data-floor="8" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_8.svg" >
		<img class="floors-stack__image floor-9" data-floor="9" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_Roof.svg" >

		<img class="floors-stack__image birds-eye floor-1" data-floor="1" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_Ground.svg" >
		<img class="floors-stack__image birds-eye floor-2" data-floor="2" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_2.svg" >
		<img class="floors-stack__image birds-eye floor-3" data-floor="3" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_3.svg" >
		<img class="floors-stack__image birds-eye floor-4" data-floor="4" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_4.svg" >
		<img class="floors-stack__image birds-eye floor-5" data-floor="5" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_5.svg" >
		<img class="floors-stack__image birds-eye floor-6" data-floor="6" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_6.svg" >
		<img class="floors-stack__image birds-eye floor-7" data-floor="7" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_7.svg" >
		<img class="floors-stack__image birds-eye floor-8" data-floor="8" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_8.svg" >
		<img class="floors-stack__image birds-eye floor-9" data-floor="9" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_Roof.svg" >
	</div>

		<div class="floors-arrows">
		<a class="floors-arrows__button up" href="javascript:void(0)"></a>
		<a class="floors-arrows__button down" href="javascript:void(0)"></a>
	</div>

	<!-- New floorplan view for mobile  -->

	<ul id="accordion" class="floorplan-dropdown mobile">
		<li>
			<h5 class="floorplan-dropdown__header">Rooftop Terrace</h5>
			<div class="floorplan-dropdown__contents">
				<img class="isometric floor-9" data-floor="9" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_Roof.svg" >
				<img class="birds-eye floor-9" data-floor="9" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_Roof.svg" >		
				<h4>11,936 RSF</h4>
				<h4>UNRIVALED VIEWS:</h4>
				<h4>MANHATTAN SKYLINE (W &amp; S)</h4>
				<h4>EAST RIVER (W &amp; S)</h4>
				<h4>DOWNTOWN BROOKLYN (E)</h4>
				<a class="button birds-eye-toggle-mobile" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-dropdown__header">Level 8</h5>
			<div class="floorplan-dropdown__contents">
				<img class="isometric floor-8" data-floor="8" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_8.svg" >
				<img class="birds-eye floor-8" data-floor="8" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_8.svg" >
				<h4>54,042 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>LARGE CENTRALIZED COLLABORATIVE SPACE</h4>
				<h4>ADJACENT TERRACE TO THE NORTH</h4>
				<h4>OUTDOOR TERRACE: 4,142 SF</h4>
				<a class="button birds-eye-toggle-mobile" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-dropdown__header">Level 7</h5>
			<div class="floorplan-dropdown__contents">
				<img class="isometric floor-7" data-floor="7" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_7.svg" >
				<img class="birds-eye floor-7" data-floor="7" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_7.svg" >
				<h4>57,356 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>LARGE CENTRALIZED COLLABORATIVE SPACE</h4>
				<h4>ADJACENT TERRACE TO THE SOUTH</h4>
				<h4>OUTDOOR TERRACE: 4,045 SF</h4>
				<a class="button birds-eye-toggle-mobile" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-dropdown__header">Level 6</h5>
			<div class="floorplan-dropdown__contents">
				<img class="isometric floor-6" data-floor="6" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_6.svg" >
				<img class="birds-eye floor-6" data-floor="6" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_6.svg" >
				<h4>60,570 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>LARGE CENTRALIZED COLLABORATIVE SPACE</h4>
				<h4>ADJACENT TERRACE TO THE NORTH</h4>
				<h4>OUTDOOR TERRACE: 2,363 SF</h4>
				<a class="button birds-eye-toggle-mobile" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-dropdown__header">Level 5</h5>
			<div class="floorplan-dropdown__contents">
				<img class="isometric floor-5" data-floor="5" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_5.svg" >
				<img class="birds-eye floor-5" data-floor="5" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_5.svg" >
				<h4>61,447 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>CENTRALIZED COLLABORATIVE SPACE</h4>
				<h4>ADJACENT TERRACES TO THE NORTH AND SOUTH</h4>
				<h4>OUTDOOR TERRACE: 4,288 SF</h4>
				<a class="button birds-eye-toggle-mobile" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-dropdown__header">Level 4</h5>
			<div class="floorplan-dropdown__contents">
				<img class="isometric floor-4" data-floor="4" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_4.svg" >
				<img class="birds-eye floor-4" data-floor="4" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_4.svg" >
				<h4>66,132 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>CENTRALIZED COLLABORATIVE SPACE</h4>
				<h4>ADJACENT TERRACES TO THE NORTH AND SOUTH</h4>
				<h4>OUTDOOR TERRACE: 6,734 SF</h4>
				<a class="button birds-eye-toggle-mobile" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-dropdown__header">Level 3</h5>
			<div class="floorplan-dropdown__contents">
				<img class="isometric floor-3" data-floor="3" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_3.svg" >
				<img class="birds-eye floor-3" data-floor="3" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_3.svg" >
				<h4>74,345 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>CENTRALIZED COLLABORATIVE SPACE</h4>
				<a class="button birds-eye-toggle-mobile" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-dropdown__header">Level 2</h5>
			<div class="floorplan-dropdown__contents">
				<img class="isometric floor-2" data-floor="2" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_2.svg" >
				<img class="birds-eye floor-2" data-floor="2" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_2.svg" >
				<h4>58,572 RSF</h4>
				<h4>15 FT OPEN CEILINGS</h4>
				<h4>2N: 29,327 RSF / 2S: 29,245 RSF</h4>
				<h4>ADJACENT TERRACES TO THE NORTH AND SOUTH</h4>
				<h4>OUTDOOR TERRACE: 6,734 SF</h4>
				<a class="button birds-eye-toggle-mobile" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
		<li>
			<h5 class="floorplan-dropdown__header">Ground Floor</h5>
			<div class="floorplan-dropdown__contents">
				<img class="isometric floor-1" data-floor="1" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25K_Floors_Ground.svg" >
				<img class="birds-eye floor-1" data-floor="1" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floor-plans/25Kent_AllFloors_B_Ground.svg" >
				<h4>43,792 RSF</h4>
				<h4>22 FT OPEN CEILINGS</h4>
				<h4>RETAIL &amp; PUBLIC PLAZAS</h4>
				<a class="button birds-eye-toggle-mobile" href="javascript:void(0)">
					<span class="button__text">Bird's-Eye View</span>
				</a>
			</div>
		</li>
	</ul>






	<!-- New floorplan view for mobile  -->
</section>

<section class="full-width-fluid navy block floorplan-fits">
	<div class="floorplan-fits__block block__text">
		<h4><?php if ( get_field('fits_header') ) echo esc_html( get_field( 'fits_header' ) ); ?></h4>
		<h2><?php if ( get_field('fits_title') ) echo esc_html( get_field( 'fits_title' ) ); ?></h2>
		<p><?php if ( get_field('fits_copy') ) echo esc_html( get_field( 'fits_copy' ) ); ?></p>
	</div>
</section>

<section class="full-width block floorplan-details">

	<div class="floorplan-details__image">
		<?php if( get_field('full_floorplan_image') ): ?>
			<img src="<?php echo esc_url( get_field('full_floorplan_image') ); ?>" />
		<?php endif; ?>
	</div>

	<div class="floorplan-details__info">

			<h4><?php if ( get_field('lg_header') ) echo esc_html( get_field( 'lg_header' ) ); ?></h4>
			<h2><?php if ( get_field('lg_title') ) echo esc_html( get_field( 'lg_title' ) ); ?></h2>
			<p><?php if ( get_field('lg_copy') ) echo esc_html( get_field( 'lg_copy' ) ); ?></p>

			<?php if( have_rows('floorplan_lg_stats') ): ?>

			<ul class="floorplan-details__stats">

			<?php while( have_rows('floorplan_lg_stats') ): the_row(); 

				$tenant = get_sub_field('tenant');
				$details = get_sub_field('details');

				?>

				<li>
					
					<?php if( $tenant ): ?>
						<span><?php echo esc_html( $tenant ); ?></span>
					<?php endif; ?>

					<?php if( $details ): ?>
						<p><?php echo esc_html( $details ); ?></p>
					<?php endif; ?>

				</li>

			<?php endwhile; ?>

			</ul>

			<?php endif; ?>

	</div>
	
</section>

<section class="full-width hr block">
	<hr class="full">
</section>

<section class="full-width block floorplan-details">

	<div class="floorplan-details__image">
		<?php if( get_field('half_floorplan_image') ): ?>
			<img src="<?php echo esc_url( get_field('half_floorplan_image') ); ?>" />
		<?php endif; ?>
	</div>

	<div class="floorplan-details__info">

			<h4><?php if ( get_field('md_header') ) echo esc_html( get_field( 'md_header' ) ); ?></h4>
			<h2><?php if ( get_field('md_title') ) echo esc_html( get_field( 'md_title' ) ); ?></h2>
			<p><?php if ( get_field('md_copy') ) echo esc_html( get_field( 'md_copy' ) ); ?></p>

			<?php if( have_rows('floorplan_md_stats') ): ?>

			<ul class="floorplan-details__stats">

			<?php while( have_rows('floorplan_md_stats') ): the_row(); 

				$tenant = get_sub_field('tenant');
				$details = get_sub_field('details');

				?>

				<li>
					
					<?php if( $tenant ): ?>
						<span><?php echo esc_html( $tenant ); ?></span>
					<?php endif; ?>

					<?php if( $details ): ?>
						<p><?php echo esc_html( $details ); ?></p>
					<?php endif; ?>

				</li>

			<?php endwhile; ?>

			</ul>

			<?php endif; ?>

	</div>
	
</section>

<section class="full-width hr block">
	<hr class="full">
</section>

<section class="full-width block floorplan-details">

	<div class="floorplan-details__image">
		<?php if( get_field('multi_floorplan_image') ): ?>
			<img src="<?php echo esc_url( get_field('multi_floorplan_image') ); ?>" />
		<?php endif; ?>
	</div>

	<div class="floorplan-details__info">

			<h4><?php if ( get_field('sm_header') ) echo esc_html( get_field( 'sm_header' ) ); ?></h4>
			<h2><?php if ( get_field('sm_title') ) echo esc_html( get_field( 'sm_title' ) ); ?></h2>
			<p><?php if ( get_field('sm_copy') ) echo esc_html( get_field( 'sm_copy' ) ); ?></p>

			<?php if( have_rows('floorplan_sm_stats') ): ?>

			<ul class="floorplan-details__stats">

			<?php while( have_rows('floorplan_sm_stats') ): the_row(); 

				$tenant = get_sub_field('tenant');
				$details = get_sub_field('details');

				?>

				<li>
					
					<?php if( $tenant ): ?>
						<span><?php echo esc_html( $tenant ); ?></span>
					<?php endif; ?>

					<?php if( $details ): ?>
						<p><?php echo esc_html( $details ); ?></p>
					<?php endif; ?>

				</li>

			<?php endwhile; ?>

			</ul>

			<?php endif; ?>

	</div>
	
</section>

<section class="full-width hr block">
	<hr class="full">
</section>

<?php

echo get_template_part('snippet-footer-cta');

get_footer();