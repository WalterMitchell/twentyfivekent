<?php 
/*
Template Name: Contact Page
*/

get_header();
?>

<section class="full-width-fluid bordered navy block contact-hero">
	<div class="contact-hero__text block__text">
		<h1 class="contact-hero__header">
			<?php if ( get_field('hero_title') ) echo esc_html(get_field('hero_title')); ?>
		</h1>	
		<p class="big contact-hero__paragraph">
			<?php if ( get_field('hero_description') ) echo esc_html(get_field('hero_description')); ?>
		</p>
	</div>
</section>

<?php 
if ( get_field('logo_image') ) $logo = get_field('logo_image');

if ( get_field('agent_1_phone_number') ) $phone_1 = esc_html(get_field('agent_1_phone_number'));
if ( get_field('agent_1_e-mail') ) $email_1 = esc_html(get_field('agent_1_e-mail'));

if ( get_field('agent_2_phone_number') ) $phone_2 = esc_html(get_field('agent_2_phone_number'));
if ( get_field('agent_2_e-mail') ) $email_2 = esc_html(get_field('agent_2_e-mail'));

if ( get_field('agent_3_phone_number') ) $phone_3 = esc_html(get_field('agent_3_phone_number'));
if ( get_field('agent_3_e-mail') ) $email_3 = esc_html(get_field('agent_3_e-mail'));

?>

<section class="full-width-fluid bordered block contact-inquiries">
	<img class="contact-inquiries__logo"src="<?php if ( isset($logo) ) echo esc_url($logo['sizes']['hero']); ?>" alt="mark">
	<hr>
	<h2 class="contact-inquiries__header"><?php if ( get_field('tenant_inquiries') ) echo esc_html(get_field('tenant_inquiries')); ?></h2>
	<p class="contact-inquiries__subhead"><?php if ( get_field('tenant_inquiries_sub') ) echo wpautop( esc_html( get_field('tenant_inquiries_sub'))); ?></p>

	<div class="col-4 contact-inquiries__col">
		<h4 class="contact-inquiries__name"><?php if ( get_field('agent_1') ) echo esc_html(get_field('agent_1')); ?></h4>
		<p class="contact-inquiries__paragraph">
			<a href="tel:+1<?php echo str_replace(".","",$phone_1); ?>" class="tel"><?php echo $phone_1; ?></a>
		</p>
		<p class="contact-inquiries__paragraph">
			<a href="mailto:<?php echo $email_1; ?>" target="_blank"><?php echo $email_1; ?></a>
		</p>
	</div>

	<div class="col-4 contact-inquiries__col">
		<h4 class="contact-inquiries__name"><?php if ( get_field('agent_1') ) echo esc_html(get_field('agent_2')); ?></h4>
		<p class="contact-inquiries__paragraph">
			<a href="tel:+1<?php echo str_replace(".","",$phone_2); ?>" class="tel"><?php echo $phone_2; ?></a>
		</p>
		<p class="contact-inquiries__paragraph">
			<a href="mailto:<?php echo $email_2; ?>" target="_blank"><?php echo $email_2; ?></a>
		</p>
	</div>

	<div class="col-4 contact-inquiries__col">
		<h4 class="contact-inquiries__name"><?php if ( get_field('agent_1') ) echo esc_html(get_field('agent_3')); ?></h4>
		<p class="contact-inquiries__paragraph">
			<a href="tel:+1<?php echo str_replace(".","",$phone_3); ?>" class="tel"><?php echo $phone_3; ?></a>
		</p>
		<p class="contact-inquiries__paragraph">
			<a href="mailto:<?php echo $email_3; ?>" target="_blank"><?php echo $email_3; ?></a>
		</p>
	</div>

</section>

<section class="full-width bordered hr block">
	<hr class="full">
</section>

<section class="full-width-fluid bordered block contact-inquiries">
	<h2 class="contact-inquiries__second contact-inquiries__header"><?php if ( get_field('press_inquiries') ) echo esc_html(get_field('press_inquiries')); ?></h2>
	<hr class="single">
	<div class="single col-4 contact-inquiries__col">
		<h4 class="contact-inquiries__name"><?php if ( get_field('press_name') ) echo esc_html(get_field('press_name')); ?></h4>
		<p class="contact-inquiries__paragraph">
			<a href="tel:+1<?php if ( get_field('press_phone_number') ) echo esc_html(get_field('press_phone_number')); ?>" class="tel"><?php if ( get_field('press_phone_number') ) echo esc_html(get_field('press_phone_number')); ?></a>
		</p>
		<p class="contact-inquiries__paragraph"> 
			<a href="mailto:<?php if ( get_field('press_email') ) echo esc_html(get_field('press_email')); ?>" target="_blank"><?php if ( get_field('press_email') ) echo esc_html(get_field('press_email')); ?></a>
		</p>
	</div>
	<div class="single col-4 contact-inquiries__col">
		<h4 class="contact-inquiries__name"><?php if ( get_field('press_name_1') ) echo esc_html(get_field('press_name_1')); ?></h4>
		<p class="contact-inquiries__paragraph">
			<a href="tel:+1<?php if ( get_field('press_phone_number_1') ) echo esc_html(get_field('press_phone_number_1')); ?>" class="tel"><?php if ( get_field('press_phone_number_1') ) echo esc_html(get_field('press_phone_number_1')); ?></a>
		</p>
		<p class="contact-inquiries__paragraph"> 
			<a href="mailto:<?php if ( get_field('press_email_1') ) echo esc_html(get_field('press_email_1')); ?>" target="_blank"><?php if ( get_field('press_email_1') ) echo esc_html(get_field('press_email_1')); ?></a>
		</p>
	</div>
</section>

<section class="full-width-fluid bordered skyblue block contact-maps">
	<div class="contact-maps__block block__text">
		<h4 class="contact-maps__header"><?php if ( get_field('location') ) echo esc_html(get_field('location')); ?></h4>
		<h2 class="contact-maps__title"><?php if ( get_field('location_title') ) echo esc_html(get_field('location_title')); ?></h2>
		<h4 class="contact-maps__address">
			<?php if ( get_field('location_address') ) echo '<a target="_blank" href="' . make_maps_url(get_field('location_address')) . '">' . esc_html(get_field('location_address')) . '</a>'; ?>
		</h4>
	</div>
</section>

<?php 
if ( get_field('full_width_image') ) $image = get_field('full_width_image');
?>

<section class="full-width-fluid image block contact-full-image">
	<img src="<?php if ( isset($image) ) echo esc_url($image['sizes']['hero']); ?>" alt="contact-full-image">
</section>

<?php
get_footer();
