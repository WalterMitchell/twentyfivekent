<?php 
/*
Template Name: Front Page
*/

get_header(); 

// All image fields
$intro_image = get_field('intro_image');
$neighborhood_image = get_field('neighborhood_background_image');
$gallery_image = get_field('gallery_image');
$culture_image = get_field('culture_image');
$retail_illustration = get_field('retail_illustration');
$hero_image = get_field('hero_image'); 

?>

<section class="full-width image big-hero block homepage-hero <?php if ( get_field('hero_video_mp4') ) echo 'has-video'; ?>" style="background-image:url('<?php if ( $hero_image ) echo esc_url( $hero_image['sizes']['big-hero'] )?>');">
	<?php if ( get_field('hero_video_mp4') || get_field('hero_video_webm') || get_field('hero_video_ogg') ) : ?>

		<video class="homepage-hero__video" muted loop>
			<?php 
			if ( get_field('hero_video_webm') ) echo '<source type="video/webm" src="' . esc_url( get_field('hero_video_webm') ) . '">';
			if ( get_field('hero_video_mp4') ) echo '<source type="video/mp4" src="' . esc_url( get_field('hero_video_mp4') ) . '">';
			if ( get_field('hero_video_ogg') ) echo '<source type="video/ogg" src="' . esc_url( get_field('hero_video_ogg') ) . '">';
			?>
		</video>

	<?php endif; ?>

	<div class="middle centered big-hero__content">
		<h1 class="big-hero__title"><?php if ( get_field('hero_title') ) echo esc_html( get_field('hero_title') ); ?></h1>

		<?php if ( get_field('popout_video') ) { ?>
			<a class="button homepage-hero__button big-hero__button overlay-toggle" data-target=".homepage-hero__popout-video" href="javascript:void(0)">
				<span class="button__text">Play Video</span>
			</a>
			<div class="homepage-hero__popout-video middle">
				<?php the_field('popout_video'); ?>
			</div>
		<?php } else { ?>
			<a class="button homepage-hero__button big-hero__button" href="<?php echo home_url('building'); ?>">
				<span class="button__text">Meet 25 Kent</span>
			</a>
		<?php } ?>
	</div>
</section>

<section class="full-width-fluid bordered navy centered block">
	<p class="big block__text"><?php if ( get_field('intro_paragraph') ) echo esc_html( get_field('intro_paragraph') ); ?></p>
</section>

<section class="half-width bordered image block" style="background-image: url('<?php if ( $intro_image ) echo esc_url( $intro_image['sizes']['large'] ); ?>')">
</section>

<section class="half-width block">
	<div class="middle centered block__text">
		<h2><?php if ( get_field('introducing_header') ) echo strip_tags( get_field('introducing_header'), '<br>' ); ?></h2>
		<div class="building-links">
			<a class="building-link" href="<?php echo home_url('building'); ?>">
				<p class="big building-link__title">The Building</p>
				<p class="building-link__description">See what 500,000+ total square feet of inspired work space looks like.</p>
			</a>
			<a class="building-link" href="<?php echo home_url('floor-plans'); ?>">
				<p class="big building-link__title">Floor Plans</p>
				<p class="building-link__description">Explore flexible floor-by-floor layouts to see how you’ll fit in.</p>
			</a>
			<a class="building-link" href="<?php echo home_url('the-team'); ?>">
				<p class="big building-link__title">The Team</p>
				<p class="building-link__description">Get to know the names and faces behind 25 Kent.</p>
			</a>
		</div>
	</div>
</section>

<section class="full-width image block with-overlay homepage-neighborhood" style="background-image: url('<?php if ( $neighborhood_image ) echo esc_url( $neighborhood_image['url'] ); ?>')">
	<div class="block__text-overlay">
		<h4>Neighborhood</h4>
		<h2><?php if ( get_field('neighborhood_header') ) echo esc_html( get_field('neighborhood_header') ); ?></h2>
		<hr>
		<?php if ( get_field('neighborhood_paragraph') ) echo wpautop( esc_html( get_field('neighborhood_paragraph') ) ); ?>
		<a class="button" href="<?php echo home_url('neighborhood'); ?>">
			<span class="button__text">View Hotspots</span>
		</a>
	</div>
</section>

<section class="clearfix">

	<div class="half-width bordered image block" style="background-image: url('<?php if ( $gallery_image ) echo esc_url( $gallery_image['sizes']['large'] ); ?>')">
	</div>

	<div class="half-width block">
		<div class="middle centered block__text">
			<h4>Gallery</h4>
			<h2><?php if ( get_field('gallery_header') ) echo esc_html( get_field('gallery_header') ); ?></h2>
			<hr>
			<?php if ( get_field('gallery_paragraph') ) echo wpautop( esc_html( get_field('gallery_paragraph') ) ); ?>
			<a class="button" href="<?php echo home_url('gallery'); ?>">
				<span class="button__text">View Gallery</span>
			</a>
		</div>
	</div>

</section>

<!-- <section class="half-width bordered image block" style="background-image: url('<?php if ( $culture_image ) echo esc_url( $culture_image['sizes']['large'] ); ?>')">
</section>

<section class="half-width block">
	<div class="middle centered block__text">
		<h4>Culture</h4>
		<h2><?php if ( get_field('culture_header') ) echo esc_html( get_field('culture_header') ); ?></h2>
		<hr>
		<?php if ( get_field('culture_paragraph') ) echo wpautop( esc_html( get_field('culture_paragraph') ) ); ?>
		<a class="button" href="<?php echo home_url('culture'); ?>">
			<span class="button__text">Find Your Kind</span>
		</a>
	</div>
</section> -->

<section class="full-width-fluid coral block homepage-retail" style="background-image: url('<?php if ( $retail_illustration ) echo esc_url( $retail_illustration['sizes']['large'] ); ?>')">
	<div class="block__text homepage-retail__text-block">
		<h4>Retail</h4>
		<h2><?php if ( get_field('retail_header') ) echo wp_kses(get_field('retail_header'), array('br' => array(),)); ?></h2>
		<hr>
		<?php if ( get_field('retail_paragraph') ) echo wpautop( esc_html( get_field('retail_paragraph') ) ); ?>
		<a class="button" href="<?php echo home_url('retail'); ?>">
			<span class="button__text">Shop Around<span>
		</a>
	</div>
</section>

<?php

echo get_template_part('snippet-footer-cta');
get_footer();