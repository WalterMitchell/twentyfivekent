<?php

$cta_header = get_field('cta_header'); 
$cta_button_text = get_field('cta_button_text'); 

echo '<section class="full-width-fluid centered block footer-cta">';
	if ( $cta_header ) echo '<h2>' . wp_kses(get_field('cta_header'), array('br' => array(),)) . '</h2>';
	echo '<hr class="footer-cta__hr">';
	if ( $cta_button_text ) echo '<a class="button footer-cta__button" href="' . home_url('contact') . '"><span class="footer-cta__button-text">' . esc_html($cta_button_text) . '</span></a>';
echo '</section>';



