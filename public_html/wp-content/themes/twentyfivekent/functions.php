<?php

// Debugging 
$debugging = false;
if ( SERVER_ENVIRONMENT === 'dev' ) $debugging = true;


//
// Stylesheets and Javascript
//

function theme_scripts_styles() {

    $version_number = '0.1';
	global $debugging;

    // Theme registration stylesheet, combined theme stylesheet and any other necessary styles
    wp_enqueue_style( 'registration-stylesheet', get_stylesheet_directory_uri() . '/style.css', $version_number );
    wp_enqueue_style( 'theme-styles', get_stylesheet_directory_uri() . '/css/theme.css', $version_number );

    // Use non-minified JS files for local dev, otherwise load single concatinated app.js
	if( $debugging )  {
		
        wp_enqueue_script( 'concatinated-js', get_stylesheet_directory_uri() . '/js/app.js', array( 'jquery' ), $version_number, true );
        // ... add more scripts as you use them and don't include them in Gulp

	} 
	else {

    	wp_enqueue_script( 'minified-js', get_stylesheet_directory_uri() . '/js/app.min.js', array( 'jquery' ), $version_number, true );

    }

}

add_action( 'wp_enqueue_scripts', 'theme_scripts_styles' );


function add_jquery_ui() {
        
    wp_enqueue_script( 'jquery-ui-accordion' );
    
}
add_action( 'wp_enqueue_scripts', 'add_jquery_ui' );


//
// General theme setup
//

function theme_setup() {

    // Adds RSS feed links to <head> for posts and comments.
    add_theme_support( 'automatic-feed-links' );

    // This theme uses wp_nav_menu() some locations
    register_nav_menus(
        array(
            'main-menu-left' => __( 'Main Menu Left' ),
            'main-menu-right' => __( 'Main Menu Right' ),
            'footer-menu-left' => __( 'Footer Menu Left' ),
            'footer-menu-center' => __( 'Footer Menu Center' ),
            'footer-menu-right' => __( 'Footer Menu Right' )
        )
    );

    //  Enable featured images for posts
    add_theme_support( 'post-thumbnails' );
    
    // set_post_thumbnail_size( 265, 180, true );
    add_image_size( 'hero', 2440, 922, true );
    add_image_size( 'big-hero', 2560, 1320, true );
}

add_action( 'after_setup_theme', 'theme_setup' );


//
// Allow SVG uploads
//

function custom_upload_mimetypes ( $mimes ) {

    $mimes['svg'] = 'image/svg+xml';
    return $mimes;

}
add_filter('upload_mimes', 'custom_upload_mimetypes');


//
// Admin CSS
//

function svg_wrapper() {
    // Add slightly darker gray background so transparency-enabled images are visible if white
    echo '<style>
        .acf-input .view img[src*=".svg"],
        .acf-input .view img[src*=".png"] {
            background: #ddd;
        }
    </style>';
}
add_action('admin_head', 'svg_wrapper');


//
// Helper function for making Google Maps URL
//

function make_maps_url( $address ) {
    $remove = array("\n", "\r\n", "\r");
    $string = str_replace($remove, ' ', $address );
    return "https://maps.google.com?q=" . htmlentities(urlencode($string));
}

// Remove WP Generator Meta Tag
remove_action('wp_head', 'wp_generator');

// Custom WP Walker
@include 'utilities/walker.php';

// Fields
@include 'fields/front-page.php';
@include 'fields/page-building.php';
@include 'fields/snippet-footer-cta.php';
@include 'fields/contact-page.php';
@include 'fields/team-page.php';
@include 'fields/floor-plans-page.php';
@include 'fields/retail-page.php';
@include 'fields/gallery-page.php';
@include 'fields/neighborhood-page.php';