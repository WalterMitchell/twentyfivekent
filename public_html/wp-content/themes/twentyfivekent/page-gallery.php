<?php
/*
Template Name: Gallery Page
*/

get_header(); 

echo get_template_part('snippet-hero-image');

?>

<section class="full-width-fluid bordered navy block">
	<div class="centered">
		<h2 class="gallery-hero__header">
			<?php if ( get_field('hero_title') ) echo esc_html(get_field('hero_title')); ?>
		</h2>	
		<p class="big gallery-hero__paragraph">
			<?php if ( get_field('hero_description') ) echo esc_html(get_field('hero_description')); ?>
		</p>
		<a href="#gallery-intro"><img class="gallery-hero__indicator" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_arrow.svg" /></a>
	</div>
</section>

<section id="gallery-intro" class="full-width bordered block">

	<div class="half-width block gallery-interiors__copy">
		<div class="middle centered block__text">
			<h4><?php if ( get_field('interiors_header') ) echo esc_html(get_field('interiors_header')); ?></h4>
			<h2><?php if ( get_field('interiors_headline') ) echo esc_html(get_field('interiors_headline')); ?></h2>
			<hr>
			<p><?php if ( get_field('interiors_copy') ) echo esc_html(get_field('interiors_copy')); ?></p>
		</div>
	</div>

	<div class="half-width block gallery-interiors__container">

	<?php if( have_rows('interiors_carousel') ): ?>
		
		<div class="gallery-interiors carousel">
		
			<?php while( have_rows('interiors_carousel') ): the_row(); 

				$image = get_sub_field('image');
				if ( $image ) echo '<img src="' . esc_url( $image ) . '">';

			endwhile; ?>
	
		</div>
	
	<?php endif; ?>

		<div class="gallery-interiors__controls slick-controls">
			<a href="javascript:void(0)" class="next"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_arrow.svg" /></a>
			<a href="javascript:void(0)" class="prev"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_arrow.svg" /></a>
		</div>

	</div>

</section>

<section class="pano-container__interior">
	<div id="interior" class="gallery-interiors__pano">
		<a href="javascript:void(0)" class="gallery-interiors__close"></a>
	</div>
	<div class="gallery-interiors__wall">
		<div class="middle gallery-interiors__views">
			<h4 class="inline">360° View</h4>
			<h2><?php if ( get_field('interiors_pano_headline') ) echo esc_html(get_field('interiors_pano_headline')); ?></h2>
			<p><?php if ( get_field('interiors_pano_copy') ) echo esc_html(get_field('interiors_pano_copy')); ?></p>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pano-arrows.png" />
		</div>
	</div>

</section>

<section class="full-width bordered block">

	<div class="half-width block gallery-exteriors__copy">
		<div class="middle centered block__text">
			<h4><?php if ( get_field('exteriors_header') ) echo esc_html(get_field('exteriors_header')); ?></h4>
			<h2><?php if ( get_field('exteriors_headline') ) echo esc_html(get_field('exteriors_headline')); ?></h2>
			<hr>
			<p><?php if ( get_field('exteriors_copy') ) echo esc_html(get_field('exteriors_copy')); ?></p>
		</div>
	</div>

	<div class="half-width block gallery-exteriors__container">

	<?php if( have_rows('exteriors_carousel') ): ?>
		
		<div class="gallery-exteriors carousel">
		
			<?php while( have_rows('exteriors_carousel') ): the_row(); 

				$image = get_sub_field('image');
				if ( $image ) echo '<img src="' . esc_url( $image ) . '">';

			endwhile; ?>
		
		</div>
	
	<?php endif; ?>

		<div class="gallery-exteriors__controls slick-controls">
			<a href="javascript:void(0)" class="next"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_arrow.svg" /></a>
			<a href="javascript:void(0)" class="prev"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_arrow.svg" /></a>
		</div>

	</div>

</section>

<section class="pano-container__exterior">
	<div id="exterior" class="gallery-exteriors__pano">
		<a href="javascript:void(0)" class="gallery-exteriors__close"></a>
	</div>

	<div class="gallery-exteriors__wall">
		<div class="middle gallery-exteriors__views">
			<h4 class="inline">360° View</h4>
			<h2><?php if ( get_field('exteriors_pano_headline') ) echo esc_html(get_field('exteriors_pano_headline')); ?></h2>
			<p><?php if ( get_field('exteriors_pano_copy') ) echo esc_html(get_field('exteriors_pano_copy')); ?></p>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pano-arrows.png" />
		</div>
	</div>

</section>

<section class="full-width bordered block">

	<div class="half-width block gallery-outdoors__copy">
		<div class="middle centered block__text">
			<h4><?php if ( get_field('outdoors_header') ) echo esc_html(get_field('outdoors_header')); ?></h4>
			<h2><?php if ( get_field('outdoors_headline') ) echo esc_html(get_field('outdoors_headline')); ?></h2>
			<hr>
			<p><?php if ( get_field('outdoors_copy') ) echo esc_html(get_field('outdoors_copy')); ?></p>
		</div>
	</div>

	<div class="half-width block gallery-outdoors__container">

	<?php if( have_rows('outdoors_carousel') ): ?>
		
		<div class="gallery-outdoors carousel">
			
			<?php while( have_rows('outdoors_carousel') ): the_row(); 

				$image = get_sub_field('image');
				if ( $image ) echo '<img src="' . esc_url( $image ) . '">';

			endwhile; ?>
	
		</div>
	
	<?php endif; ?>

		<div class="gallery-outdoors__controls slick-controls">
			<a href="javascript:void(0)" class="next"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_arrow.svg" /></a>
			<a href="javascript:void(0)" class="prev"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25K_arrow.svg" /></a>
		</div>

	</div>

</section>

<section class="pano-container__outdoor">
	<div id="outdoor" class="gallery-outdoors__pano">
		<a href="javascript:void(0)" class="gallery-outdoors__close"></a>
	</div>

	<div class="gallery-outdoors__wall">
		<div class="middle gallery-outdoors__views">
			<h4 class="inline">360° View</h4>
			<h2><?php if ( get_field('outdoors_pano_headline') ) echo esc_html(get_field('outdoors_pano_headline')); ?></h2>
			<p><?php if ( get_field('outdoors_pano_copy') ) echo esc_html(get_field('outdoors_pano_copy')); ?></p>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/pano-arrows.png" />
		</div>
	</div>

</section>

<?php

echo get_template_part('snippet-footer-cta');

get_footer();