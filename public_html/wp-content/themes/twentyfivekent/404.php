<?php
/*
Template Name: 404 Page
*/

get_header(); 

?>

<section class="notfound-message">
	<div class="centered block__text">
		<img class="notfound-message__image" src="<?php echo get_stylesheet_directory_uri(); ?>/images/404-image.svg" />
		<h1>404</h1>
		<hr class="notfound-message__hr">
		<p>Sorry, we had the map upside-down.</p>
		<a class="button" href="<?php echo get_home_url(); ?>">
			<span class="button__text">Go Home</span>
		</a>
	</div>
</section>

<?php

get_footer();