<?php
/*
Template Name: Retail Page
*/

get_header(); 

echo get_template_part('snippet-hero-image');

?>

<section id="retail-intro" class="block">
	<div class="centered block__text">
		<h1><?php if ( get_field('intro_title') ) echo esc_html( get_field( 'intro_title' ) ); ?></h1>
		<p class="big"><?php if ( get_field('intro_copy') ) echo esc_html( get_field('intro_copy' ) ); ?></p>
	</div>
</section>

<section class="bordered gray block">
	<div class="centered retail-amenities">

		<h4><?php if ( get_field('amenities_header') ) echo esc_html( get_field( 'amenities_header' ) ); ?></h4>
		<h2><?php if ( get_field('amenities_headline') ) echo esc_html( get_field( 'amenities_headline' ) ); ?></h2>

		<?php if( have_rows('amenities') ): ?>

		<ul class="retail-amenities__features">

		<?php while( have_rows('amenities') ): the_row(); 

			$icon = get_sub_field('icon');
			$feature = get_sub_field('feature');

			?>

			<li class="<?php echo esc_html($feature); ?>">
				
			<?php if( $icon ): ?>
				<div class="retail-amenities__image">
					<img src="<?php echo esc_url($icon); ?>" />
				</div>
			<?php endif; ?>

			<?php if( $feature ): ?>
				<h5><?php echo esc_html($feature); ?></h5>
			<?php endif; ?>

			</li>

		<?php endwhile; ?>

		</ul>

		<?php endif; ?>

	</div>
	<div class="full-width hr inner">
		<hr class="navy full">
	</div>
	<div class="centered retail-manufacturing">
		<h4><?php if ( get_field('manufacturing_header') ) echo esc_html( get_field( 'manufacturing_header' ) ); ?></h4>
		<h2><?php if ( get_field('manufacturing_headline') ) echo esc_html( get_field( 'manufacturing_headline' ) ); ?></h2>
		<p class="big"><?php if ( get_field('manufacturing_copy') ) echo esc_html( get_field('manufacturing_copy' ) ); ?></p>
	</div>
</section>

<?php

echo get_template_part('snippet-footer-cta');

get_footer();