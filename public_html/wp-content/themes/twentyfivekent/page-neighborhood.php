<?php 
/*
Template Name: Neighborhood Page
*/

get_header(); 

// All image fields
$intro_image = get_field('intro_image');
$hero_image = get_field('hero_image'); 
$dining_image = get_field('dining_image');
$dining_half_image = get_field('dining_half_image');
$parks_image = get_field('parks_image');
$parks_half_image = get_field('parks_half_image');
$entertainment_image = get_field('entertainment_image');
$entertainment_half_image = get_field('entertainment_half_image');
$hotspots_half_image = get_field('hotspots_half_image');
$transit_half_image = get_field('transit_half_image');

?>

<section class="full-width image big-hero block neighborhood-hero" style="background-image:url('<?php if ( $hero_image ) echo esc_url( $hero_image['sizes']['big-hero'] )?>');">
	<?php if ( get_field('hero_video_mp4') || get_field('hero_video_webm') || get_field('hero_video_ogg') ) : ?>

		<video class="neighborhood-hero__video" autoplay muted loop>
			<?php 
			if ( get_field('hero_video_mp4') ) echo '<source type="video/mp4" src="' . esc_url( get_field('hero_video_mp4') ) . '">';
			if ( get_field('hero_video_webm') ) echo '<source type="video/webm" src="' . esc_url( get_field('hero_video_webm') ) . '">';
			if ( get_field('hero_video_ogg') ) echo '<source type="video/ogg" src="' . esc_url( get_field('hero_video_ogg') ) . '">';
			?>
		</video>

	<?php endif; ?>

	<div class="middle centered big-hero__content">
		<h1 class="big-hero__title"><?php if ( get_field('hero_title') ) echo esc_html( get_field('hero_title') ); ?></h1>
	</div>

	<div class="neighborhood-hero__buttons">
		<a class="button neighborhood-hotspots__launch big-hero__button" href="javascript:void(0)">
			<span class="button__text">Hotspots Map</span>
		</a>
		<a class="button neighborhood-transit__launch big-hero__button" href="javascript:void(0)">
			<span class="button__text">Transit Map</span>
		</a>
	</div>
</section>

<section class="full-width-fluid bordered navy centered block neighborhood-intro">
	<p class="big block__text"><?php if ( get_field('intro_paragraph') ) echo esc_html( get_field('intro_paragraph') ); ?></p>
</section>

<section class="full-width image big-hero block neighborhood-dining__hero" style="background-image:url('<?php if ( $dining_image ) echo esc_url( $dining_image['sizes']['big-hero'] )?>');">
	<?php if ( get_field('dining_video_mp4') || get_field('dining_video_webm') || get_field('dining_video_ogg') ) : ?>

		<video class="neighborhood-hero__video" autoplay muted loop>
			<?php 
			if ( get_field('dining_video_mp4') ) echo '<source type="video/mp4" src="' . esc_url( get_field('dining_video_mp4') ) . '">';
			if ( get_field('dining_video_webm') ) echo '<source type="video/webm" src="' . esc_url( get_field('dining_video_webm') ) . '">';
			if ( get_field('dining_video_ogg') ) echo '<source type="video/ogg" src="' . esc_url( get_field('dining_video_ogg') ) . '">';
			?>
		</video>

	<?php endif; ?>

	<div class="middle centered big-hero__content">
		<h1 class="big-hero__title"><?php if ( get_field('dining_title') ) echo esc_html( get_field('dining_title') ); ?></h1>
	</div>

</section>

<section class="half-width block neighborhood-dining__copy">
	<div class="middle centered block__text">
		<h2><?php if ( get_field('dining_headline') ) echo esc_html(get_field('dining_headline')); ?></h2>
		<hr>
		<p><?php if ( get_field('dining_paragraph') ) echo esc_html(get_field('dining_paragraph')); ?></p>
	</div>
</section>
<section class="half-width bordered image block" style="background-image: url('<?php if ( $dining_half_image ) echo esc_url( $dining_half_image['sizes']['large'] ); ?>')">
</section>

<section class="full-width image big-hero block neighborhood-parks__hero" style="background-image:url('<?php if ( $parks_image ) echo esc_url( $parks_image['sizes']['big-hero'] )?>');">
	<?php if ( get_field('parks_video_mp4') || get_field('parks_video_webm') || get_field('parks_video_ogg') ) : ?>

		<video class="neighborhood-hero__video" autoplay muted loop>
			<?php 
			if ( get_field('parks_video_mp4') ) echo '<source type="video/mp4" src="' . esc_url( get_field('parks_video_mp4') ) . '">';
			if ( get_field('parks_video_webm') ) echo '<source type="video/webm" src="' . esc_url( get_field('parks_video_webm') ) . '">';
			if ( get_field('parks_video_ogg') ) echo '<source type="video/ogg" src="' . esc_url( get_field('parks_video_ogg') ) . '">';
			?>
		</video>

	<?php endif; ?>

	<div class="middle centered big-hero__content">
		<h1 class="big-hero__title"><?php if ( get_field('parks_title') ) echo esc_html( get_field('parks_title') ); ?></h1>
	</div>
</section>

<section class="half-width block neighborhood-parks__copy">
	<div class="middle centered block__text">
		<h2><?php if ( get_field('parks_headline') ) echo esc_html(get_field('parks_headline')); ?></h2>
		<hr>
		<p><?php if ( get_field('parks_paragraph') ) echo esc_html(get_field('parks_paragraph')); ?></p>
	</div>
</section>
<section class="half-width bordered image block float-right" style="background-image: url('<?php if ( $parks_half_image ) echo esc_url( $parks_half_image['sizes']['large'] ); ?>')">
</section>

<section class="full-width image big-hero block neighborhood-entertainment__hero" style="background-image:url('<?php if ( $entertainment_image ) echo esc_url( $entertainment_image['sizes']['big-hero'] )?>');">
	<?php if ( get_field('entertainment_video_mp4') || get_field('entertainment_video_webm') || get_field('entertainment_video_ogg') ) : ?>

		<video class="neighborhood-hero__video" autoplay muted loop>
			<?php 
			if ( get_field('entertainment_video_mp4') ) echo '<source type="video/mp4" src="' . esc_url( get_field('entertainment_video_mp4') ) . '">';
			if ( get_field('entertainment_video_webm') ) echo '<source type="video/webm" src="' . esc_url( get_field('entertainment_video_webm') ) . '">';
			if ( get_field('entertainment_video_ogg') ) echo '<source type="video/ogg" src="' . esc_url( get_field('entertainment_video_ogg') ) . '">';
			?>
		</video>

	<?php endif; ?>

	<div class="middle centered big-hero__content">
		<h1 class="big-hero__title"><?php if ( get_field('entertainment_title') ) echo esc_html( get_field('entertainment_title') ); ?></h1>
	</div>

</section>

<section class="half-width block neighborhood-entertainment__copy">
	<div class="middle centered block__text">
		<h2><?php if ( get_field('entertainment_headline') ) echo esc_html(get_field('entertainment_headline')); ?></h2>
		<hr>
		<p><?php if ( get_field('entertainment_paragraph') ) echo esc_html(get_field('entertainment_paragraph')); ?></p>
	</div>
</section>
<section class="half-width bordered image block" style="background-image: url('<?php if ( $entertainment_half_image ) echo esc_url( $entertainment_half_image['sizes']['large'] ); ?>')">
</section>



<section class="full-width-fluid navy block neighborhood-maps__intro">
	<div class="neighborhood-maps__block block__text">
		<h4><?php if ( get_field('maps_intro_header') ) echo esc_html( get_field( 'maps_intro_header' ) ); ?></h4>
		<h2><?php if ( get_field('maps_intro_title') ) echo esc_html( get_field( 'maps_intro_title' ) ); ?></h2>
	</div>
</section>

<section class="clearfix">

	<div class="half-width bordered image block float-right" style="background-image: url('<?php if ( $hotspots_half_image ) echo esc_url( $hotspots_half_image['sizes']['large'] ); ?>')">
	</div>

	<div class="half-width block neighborhood-hotspots__copy">
		<div class="middle centered block__text">
			<h2>Hotspots Map</h2>
			<hr>
			<p>So much to do, so little time. Use our handy hotspots map to make sure you see it all.</p>
			<a class="neighborhood-hotspots__launch button" href="javascript:void(0);">
				<span class="button__text">Open Map</span>
			</a>
		</div>
	</div>

</section>

<section id="hotspots" class="neighborhood-hotspots">
	<?php if ( !wp_is_mobile() ) { ?>
	<div class="neighborhood-hotspots__parent">
		<div class="neighborhood-hotspots__container panzoom-container" style="transform: scale(1.2) matrix(1, 0, 0, 1, 0, -300); -webkit-transform: scale(1.2) matrix(1, 0, 0, 1, 0, -300);">
			<?php echo get_template_part('snippet-hotspots-svg'); ?>
			<?php echo get_template_part('snippet-hotspot-map'); ?>
			<img class="neighborhood-hotspots__map" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent-hotspots-map.svg" />
			<div class="neighborhood-hotspots__tooltip"><p></p></div>
		</div>
	</div>
	<div class="neighborhood-hotspots__filters">
		<ul>

			<li class="active">
				<a class="all" href="javascript:void(0);">
					<img class="color" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_All_color.svg" />
					<img class="blue" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_All_blue.svg" />
				</a>
			</li>

			<li>
				<a class="coffee" href="javascript:void(0);" data-filter="6">
					<img class="color" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Caffeine_color.svg" />
					<img class="blue" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Caffeine_blue.svg" />
					<span>Caffeine</span>
				</a>
			</li>

			<li>
				<a class="music" href="javascript:void(0);" data-filter="5">
					<img class="color" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Music_color.svg" />
					<img class="blue" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Music_blue.svg" />
					<span>Live Music</span>
				</a>
			</li>

			<li>
				<a class="art" href="javascript:void(0);" data-filter="4">
					<img class="color" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Art_color.svg" />
					<img class="blue" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Art_blue.svg" />
					<span>Art Galleries</span>
				</a>
			</li>

			<li>
				<a class="food" href="javascript:void(0);" data-filter="3">
					<img class="color" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Eats_color.svg" />
					<img class="blue" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Eats_blue.svg" />
					<span>Eats</span>
				</a>
			</li>

			<li>
				<a class="drinks" href="javascript:void(0);" data-filter="2">
					<img class="color" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Bars_color.svg" />
					<img class="blue" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Bars_blue.svg" />
					<span>Bars</span>
				</a>
			</li>

			<li>
				<a class="hotels" href="javascript:void(0);" data-filter="1">
					<img class="color" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Hotels_color.svg" />
					<img class="blue" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Hotels_blue.svg" />
					<span>Hotels</span>
				</a>
			</li>

			<div class="neighborhood-hotspots__close"></div>
		</ul>
	</div>

	<div class="neighborhood-hotspots__buttons">
		<a href="javascript:void(0);" class="zoom-in">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Map_plus.svg" />
		</a>
		<a href="javascript:void(0);" class="zoom-out">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Map_minus.svg" />
		</a>
	</div>
	<?php } ?>
</section>

<section class="full-width hr block">
	<hr class="full">
</section>

<section class="clearfix">

	<div class="half-width bordered image block float-right" style="background-image: url('<?php if ( $transit_half_image ) echo esc_url( $transit_half_image['sizes']['large'] ); ?>')">
	</div>

	<div class="half-width block neighborhood-transit__copy">
		<div class="middle centered block__text">
			<h2>Transit Map</h2>
			<hr>
			<p>Everything is easy to get to from 25 Kent. Use our transit map to see all your options.</p>
			<a class="neighborhood-transit__launch button" href="javascript:void(0);">
				<span class="button__text">Open Map</span>
			</a>
		</div>
	</div>

</section>

<section id="transit" class="neighborhood-transit">
	<?php if ( !wp_is_mobile() ) { ?>
	<div class="neighborhood-transit__parent">
		<div class="neighborhood-transit__container panzoom-container" style="transform: scale(1.2) matrix(1, 0, 0, 1, 0, -80); -webkit-transform: scale(1.2) matrix(1, 0, 0, 1, 0, -80);">
			<img class="neighborhood-transit__map" src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Transit_Map.svg" />
		</div>
	</div>

	<div class="neighborhood-transit__buttons">
		<a href="javascript:void(0);" class="zoom-in">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Map_plus.svg" />
		</a>
		<a href="javascript:void(0);" class="zoom-out">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/25Kent_Hotspot_Map_minus.svg" />
		</a>
	</div>

	<div class="neighborhood-transit__close"></div>
	<?php } ?>
</section>

<section class="full-width hr block">
	<hr class="full">
</section>

<?php

echo get_template_part('snippet-footer-cta');
get_footer();