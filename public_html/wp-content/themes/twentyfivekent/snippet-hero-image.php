<?php

$hero_image = get_field('hero_image'); 

if ( $hero_image ) {

	$url = esc_url($hero_image['sizes']['hero']);

}

echo '<section class="full-width bordered image hero block" style="background-image:url('.$url.');">
</section>';