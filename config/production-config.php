<?php
die('set credentials in config/production-config.php');
define('DB_NAME',            '');
define('DB_USER',            '');
define('DB_PASSWORD',        '');
define('DB_HOST',            'localhost');
define('WP_HOME',            'http://twentyfivekent.com');
define('WP_SITEURL',         'http://twentyfivekent.com/wp'); // don't forget the /wp!
define('SERVER_ENVIRONMENT', 'prod');